//
//  ViewController.swift
//  WhitehouseJSON_069
//
//  Created by cice on 8/3/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UITableViewController {
    var petitions = [[String : String]]()
    var buscarPelicula = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let urlString = "https://www.omdbapi.com/?s=\(buscarPelicula)&?page=50&apikey=d698e272"
        //Comprobamos si nos devuelve resultados y si la URL es correcta
       
        if let url = URL(string: urlString){
            if let data = try? Data(contentsOf: url){
                //Obtenemos los resultados!!
                let json = try? JSON(data: data)
                //print(json)
                //let statusCode = json?["metadata"]["responseInfo"]["status"].intValue
                //print("Status Code: \(statusCode)")
                    //Parseamos el JSON completo
               parsearJSON(json: json!)
            }
        }
    }
    func parsearJSON(json: JSON){
        //title
        //body
        //signatureCoutn
        
        print(json)
        //var titulos = [String]()
        let resultados = json["Search"].arrayValue
        for resultado in resultados {
            let titulo = resultado["Title"].stringValue
            let descripcion = resultado["Plot"].stringValue
            let poster = resultado["Poster"].stringValue
            let imdbID = resultado["imdbID"].stringValue
            let peticionDict = ["poster": poster, "titulo": titulo, "descripcion" : descripcion, "imdbID" : imdbID]
            petitions.append(peticionDict)
        }
        
        /*
        let titulo = json["Title"].stringValue
        let descripcion = json["Plot"].stringValue
        let poster = json["Poster"].stringValue
        let peticionDict = ["poster": poster, "titulo": titulo, "descripcion" : descripcion, ]
        petitions.append(peticionDict)
        */
        // print(signatureCount)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return petitions.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.detailTextLabel?.text = "Pelicula"
        cell.textLabel?.text = petitions[indexPath.row]["titulo"]
        //cell.detailTextLabel?.text = petitions[indexPath.row]["descripcion"]
        let posterUrl = petitions[indexPath.row]["poster"]
        if let url = NSURL(string: posterUrl ?? "https://estaticos.muyinteresante.es/media/cache/760x570_thumb/uploads/images/article/5536592a70a1ae8d775df846/dia-del-mono.jpg") {
            if let data = NSData(contentsOf: url as URL) {
                
                let image = self.resizeImage(image: UIImage(data: data as Data)!, targetSize: CGSize(width: 200.0, height: 200.0))
                    
                cell.imageView?.image = image
            }
        }
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "detalle"){
            let controladorDestino = segue.destination as! DetalleViewController
            if let indexPath = tableView.indexPathForSelectedRow{
                
                controladorDestino.descripcionTitulo = "\(self.petitions[indexPath.row]["titulo"] ?? "No hay titulo")"
                print("DescripcionTitulo:" + controladorDestino.descripcionPelicula)
                
                controladorDestino.urlImangenPelicula = petitions[indexPath.row]["poster"]!
            print("urlDescripcionPelicula:" + controladorDestino.urlImangenPelicula)
                
                controladorDestino.descripcionPelicula = "\(obtenerDescripcion(id: petitions[indexPath.row]["imdbID"]!))"
            print("DescripcionPelicula:" + controladorDestino.descripcionPelicula)
               
                }
        }
        
    }
    
    func obtenerDescripcion(id:String) -> String{
    
        let urlString = "https://www.omdbapi.com/?i=\(id)&apikey=d698e272"
        //Comprobamos si nos devuelve resultados y si la URL es correcta
        var descripcion = ""
        if let url = URL(string: urlString){
                if let data = try? Data(contentsOf: url){
                    //Obtenemos los resultados!!
                    let json = try? JSON(data: data)
       
                    //Parseamos el JSON completo
                    descripcion = json!["Plot"].stringValue
                    }
            }
            return descripcion
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
   
    
}

