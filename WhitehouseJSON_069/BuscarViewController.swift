//
//  BuscarViewController.swift
//  WhitehouseJSON_069
//
//  Created by Diego Angel Fernandez Garcia on 14/3/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit

class BuscarViewController: UIViewController {

    @IBOutlet weak var buscarText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "textBuscar"){
            let controladorDestino = segue.destination as! ViewController
            controladorDestino.buscarPelicula = buscarText.text!
            
            
        }
    }

}
