//
//  DetalleViewController.swift
//  WhitehouseJSON_069
//
//  Created by Diego Angel Fernandez Garcia on 14/3/19.
//  Copyright © 2019 cice. All rights reserved.
//

import UIKit

class DetalleViewController: UIViewController {
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var descripcion: UILabel!
    @IBOutlet weak var titulo: UILabel!
    
    var descripcionPelicula = "descripcion"
    var urlImangenPelicula = "url"
    var descripcionTitulo = "titulo"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descripcion.text = descripcionPelicula
        titulo.text = descripcionTitulo
        
        if let url = NSURL(string: urlImangenPelicula) {
            if let data = NSData(contentsOf: url as URL) {
                imagen.image = UIImage(data: data as Data)
            }
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
